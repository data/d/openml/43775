# OpenML dataset: Articles-From-Buzzfeed-2020

https://www.openml.org/d/43775

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was created by our in house teams at PromptCloud(https://www.promptcloud.com/) and DataStock(https://datastock.shop/). We have about 5K samples in this dataset. You can download the full dataset here(https://app.datastock.shop/?site_name=Articles_From_BuzzFeed_2020). We have a 30 discount on all datasets in our data repository. Feel free to head over to DataStock(https://datastock.shop/) and avail the discount. 
Content
This dataset contains the following:
Total Records Count :: 14831 Domain Name: buzzfeed.com Date Range: 01st Jan 2020 - 30th Apr 2020  File Extension :: csv
Available Fields: Uniq Id, Crawl Timestamp, Title Headline, Short Description Sub Headline, Content Body, Author, Date And Time Of Posting, Image Urls
Acknowledgements
We wouldn't be here without the help of our web scraping and data mining experts at PromptCloud and DataStock. 
Inspiration
The inspiration for this dataset came from Buzzfeed itself. We thought long and hard about the informative articles that we have on Buzzfeed. So we came up with a dataset for it.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43775) of an [OpenML dataset](https://www.openml.org/d/43775). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43775/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43775/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43775/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

